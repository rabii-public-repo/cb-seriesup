package com.seriesup;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/*
 * @see https://codingbat.com/prob/p104090
 */
public class SeriesUp {

	public int[] seriesUp(int n) {
		Map<Integer, Integer[]> map = new HashMap<>();
		for (int i = 1; i <= n; i++) {
			Integer[] a = new Integer[i];
			for (int j = 0; j < a.length; j++) {
				a[j] = j + 1;
			}
			map.put(i, a);
		}
		int size = 0;
		for (Integer[] a : map.values()) {
			size += a.length;
		}
		int array[] = new int[size];
		int j = 0;
		for (Integer[] a : map.values()) {
			for (int i = 0; i < a.length; i++) {
				array[j] = a[i];
				j++;
			}
		}
		return array;
	}

}
